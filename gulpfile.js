'use strict';

var gulp = require('gulp'),
	concat = require('gulp-concat'),
	cssmin = require('gulp-cssmin'),
	plumber = require('gulp-plumber'),
	wiredep = require('wiredep').stream,
	useref = require('gulp-useref'),
	browserSync = require('browser-sync'),
    reload = browserSync.reload,
    svgstore = require('gulp-svgstore'),
    sass = require('gulp-sass'),
    uglify = require('gulp-uglify'),
    gulpif = require('gulp-if'),
    jade = require('gulp-jade'),
    imagemin = require('gulp-imagemin'),
    spritesmith = require('gulp.spritesmith'),
    autoprefixer = require('gulp-autoprefixer'),
    merge = require('merge-stream'),
    buffer = require('vinyl-buffer'),
    clean = require('gulp-clean'),
    mainBowerFiles = require('main-bower-files'),
    filter = require('gulp-filter'),
    affected = require('gulp-jade-find-affected'),
    jadeInheritance = require('gulp-jade-inheritance'),
    changed = require('gulp-changed'),
    del = require('del'),
    globbing = require('gulp-css-globbing'),
    fs = require('fs'),
	rename = require('gulp-rename');

var paths = {
	app: 'app',
	build: 'build',
	buildCreate: [
		'dev/**',
		'!**/tmp{,/**}',
	],
	dev: 'dev',
	// scssComponents: 'app/components/**/*.scss',
	scss: ['app/scss/*.scss','app/components/**/*.scss'],
	vendorSass: 'app/scss/vendor/*.scss',
	vendorCss: 'dev/css/tmp/',
	jadeWatch: [
		'app/templates/**/*.jade',
		'app/components/**/*.jade'
	],
	jade: 'app/templates/*.jade',
	svg: 'app/img/svg/*.svg',
	img: 'app/img',
	js: 'app/js/*.js',
	fonts: 'app/fonts/**/*',
	sprite: 'app/img/sprite/*.png',
	create: [
		'app/img/**',
		'!app/img/sprite{,/**}',
		'app/js/**',
		'app/fonts/**',
	],
	bower: 'bower_components'
};

// get sprite: 'app/img/sprite/*.png' must have
gulp.task('sprite', ['sprite:remove'], function () {
	var spriteData = gulp.src(paths.sprite).pipe(spritesmith({
		imgName: 'sprite.png',
		cssName: '_sprite.scss',
		padding: 3,
		imgPath: '../img/sprite.png',
		algorithm: 'top-down'
	}));

	var imgStream = spriteData.img
	    .pipe(buffer())
	    .pipe(imagemin())
	    .pipe(gulp.dest(paths.app+'/img'));

	var imgStreamDev = spriteData.img
	    .pipe(buffer())
	    .pipe(imagemin())
	    .pipe(gulp.dest(paths.dev+'/img'));


	var cssStream = spriteData.css
		.pipe(gulp.dest(paths.app+'/scss'));

	return merge(imgStream, imgStreamDev, cssStream);
});

// func css
var css = function(path) {
	var path = path || paths.dev;

	gulp.src('app/scss/base.scss')
		.pipe(plumber())
		.pipe(globbing({
        	extensions: ['.scss']
    	}))
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: true
		}))
		.pipe(concat('custom.css'))
		// .pipe(cssmin())
		.pipe(gulp.dest(path+'/css'))
		.pipe(reload({stream:true}));
};

// get css
gulp.task('scss', function() {
	css();
});

// vendor sass to css
gulp.task('vendors:toCss', function () {
	gulp.src(paths.vendorSass)
		.pipe(plumber())
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: true
		}))
		.pipe(concat('vendor.css'))	
		.pipe(gulp.dest(paths.vendorCss));
});

// func vendors
var vendors = function(min, path) {
	if(min) var  min = true;
	var path = path || paths.dev;
	var intervalID = setInterval(function() {
		fs.stat(paths.vendorCss+'vendor.css', function(err, stat) {
		    if(err == null) {
		        // console.log('File exists');
				var bowerSrc = gulp.src(mainBowerFiles());
				var bowerSrcCss = mainBowerFiles();
					bowerSrcCss.unshift(paths.vendorCss+'vendor.css')
					bowerSrcCss = gulp.src(bowerSrcCss);

			    var js = bowerSrc
			        .pipe(filter('**/*.js'))
			        .pipe(gulpif(min === true, uglify()))
			        .pipe(concat('vendors.min.js'));

			    var css = bowerSrcCss
			        .pipe(filter('**/*.css'))
			        .pipe(gulpif(min === true, cssmin()))

			        .pipe(concat('vendors.min.css'));        

			    var jsPath = js.pipe(gulp.dest(path+'/js/')),
			    	cssPath = css.pipe(gulp.dest(path+'/css/'));

			    clearInterval(intervalID);

			    return merge(js,css, jsPath, cssPath);
		    }
		});		
	}, 10);
}

// get vendors
gulp.task('vendors', ['vendors:toCss'], function () {
	vendors();
});

// funct html
var html = function(path) {
	var path = path || paths.dev;

    gulp.src(paths.jade)
    	.pipe(plumber())
    	.pipe(filter(function (file) {
            return !/\/_/.test(file.path) && !/^_/.test(file.relative);
        }))
    	.pipe(jade({
			pretty: true
		}))
	    .pipe(gulp.dest(path+'/'))
	    .pipe(reload({stream:true}));
};

// get html
gulp.task('jade', function() {
	html();
});

// funct js
var js = function(path) {
	var path = path || paths.dev;

    gulp.src(paths.js)
    	.pipe(plumber())
	    .pipe(gulp.dest(path+'/js'))
	    .pipe(reload({stream:true}));
};

// reload js
gulp.task('js', function() {
	js();
});

// fonts
gulp.task('fonts', function() {
    gulp.src(paths.fonts)
    	.pipe(plumber())
	    .pipe(gulp.dest(paths.dev+'/fonts'))
	    .pipe(reload({stream:true}));
});

// save only changed files
// gulp.task('jade:changed', function() {
//     return gulp.src('app/templates/*.jade')
//  		.pipe(plumber())
//         //only pass unchanged *main* files and *all* the partials 
//         .pipe(changed(paths.dev, {extension: '.html'}))
 
//         //find files that depend on the files that have changed 
//         .pipe(jadeInheritance({basedir: 'app/templates/'}))
 
//         //filter out partials (folders and files starting with "_" ) 
//         .pipe(filter(function (file) {
//             return !/\/_/.test(file.path) && !/^_/.test(file.relative);
//         }))
 
//         //process jade templates 
//         .pipe(jade({
//         	pretty: true
//         }))
//  		.pipe(reload({stream:true}))
//         //save all the files 
//         .pipe(gulp.dest(paths.dev+'/'));
// });

// server
gulp.task('server', function() {
  browserSync({
    server: {
      baseDir: paths.dev+'/'
    },
    port: 8080,
    open: true,
    notify: false
  });
});

// remove
gulp.task('sprite:remove', function() {
	var cleanSprite = gulp.src([paths.dev+'/img/sprite.png', paths.app+'/img/sprite.png', paths.app+'/scss/_sprite.scss'], {read: false})
		.pipe(clean());

	return merge(cleanSprite);
});

gulp.task('dev:remove', function() {
	del(paths.dev);
});


gulp.task('build:remove', function() {
	gulp.src(paths.build)
		.pipe(clean())
});

// create
gulp.task('dev:create', ['scss','vendors','jade'], function() {
	gulp.src(paths.create, {base: ''+paths.app+'/'})
		.pipe(gulp.dest(paths.dev+'/'))
});

// gulp.task('build:create', ['dev:create'], function() {
// 	setTimeout(function() {
// 		gulp.src(paths.buildCreate)
// 			.pipe(gulp.dest(paths.build+'/'))
// 	}, 100);
// });

gulp.task('build:create', ['vendors:toCss'], function() {
	setTimeout(function() {
		vendors('min', 'build');
		html('build');
		js('build');
		css('build');
		
		gulp.src(paths.create, {base: ''+paths.app+'/'})
			.pipe(gulp.dest(paths.build+'/'))
	}, 100);
});



// watchers
gulp.task('watcher', function() {
	gulp.watch(paths.scss, ['scss']);
	gulp.watch(paths.jadeWatch, ['jade']);
	gulp.watch(paths.js, ['js']);
	gulp.watch(paths.fonts, ['fonts']);
	// gulp.watch(paths.svg, ['svgsprite']);
	gulp.watch(paths.img+'/sprite/**', ['sprite']);
});


gulp.task('default', ['vendors','jade','watcher','server']);