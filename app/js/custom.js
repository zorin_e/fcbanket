console.log('hello!');

$(function() {
	var swiperLg = new Swiper('.swiper-img--block .swiper-container', {
	    speed: 300
	});

	var swiperSm = new Swiper('.people-item__img .swiper-container', {
	    speed: 300,
	    nextButton: '.people-item__img .swiper-button-next',
        prevButton: '.people-item__img .swiper-button-prev',
	}); 

	// show-hide
	$('.show-hide').click(function() {
		var t = $(this).parent();

		if (!t.find('.hide').hasClass('show')) {
			$(this).find('span').text($(this).data('text').hide);
			t.find('.hide').addClass('show');
		}
		else {
			$(this).find('span').text($(this).data('text').show);
			t.find('.hide').removeClass('show');
		}
	});

	// photo list
	if($('.grid').length > 0) {
		$('.grid').masonry({
			itemSelector: '.grid-item'
		});
	}

	// tabs
	$('.tabs__item').click(function() {
		var  index = $(this).index();
		$(this).parent().find('.tabs__item--active').removeClass('tabs__item--active');
		$(this).addClass('tabs__item--active');

		$(this).parents('.tabs').find('.tabs__content .tabs__content-item').stop().slideUp('slow').eq(index).stop().slideDown('slow');
	});

	// tabs rules
	$('.tabs-rules .tabs-rules__title').click(function() {
		var next = $(this).next();
		
		if(next.is(':hidden')) {
			next.slideDown('slow');
		}
		else {
			next.slideUp('slow');
		}
	});

	$('.tabs__prices-item').click(function() {
		$(this).parent().find('.tabs__prices-text').removeClass('tabs__prices-text--active');
		$(this).find('.tabs__prices-text').addClass('tabs__prices-text--active');
		$(this).parent().find('.tabs__inner-list').stop().slideUp('fast');
		$(this).find('.tabs__inner-list').stop().slideDown('fast');
	});

	// select all
	$('.table--head .form__checkbox').click(function() {
		$(this).parents('.table-wrapper').find('.table--messages .form__checkbox input').click();
	});

	// plus
	$('.form__plus').click(function() {
		var input = $(this).parent().find('.form__input:first').clone().removeAttr('placeholder').removeAttr('value');
		input.insertAfter($(this).parent().find('.form__input:last'));
	});

	// modals
	$('body').on('click', '[data-modal]', function() {
		$('.modal-wrapper').hide();
		$($(this).data('modal')).show();
	});

	// $('.modal-start__item').click(function() {
	// 	if($(this).is('data-modal')) {
	// 		$('.modal .modal-wrapper').slideUp('slow');
	// 		$($(this).data('modal')).slideDown('slow');
	// 	}
	// });	

	$('body').on('click', '[data-modal-own]', function() {
		if($(this).data('modal-own').position == 'right') {
			var top = $(this).position().top,
				t = $(this),
				left = $(this).position().left+$(this).outerWidth()+30;

			$('.modal-own__content').hide();
			$('.modal-own').show();
			$($(this).data('modal-own').modal).show(function() {
				// console.log($('.modal-own').height());
				$('.modal-own').css({'top': top-($('.modal-own').height()/2)+'px','left': left+'px', opacity: 1});
			});
		}
		else {
			var top = $(this).offset().top+$(this).outerHeight()+15,
				t = $(this),
				left = $(this).offset().left+$(this).outerWidth()/2;

			console.log($(this).offset().top);
			$('.modal-own__content').hide();
			$('.modal-own').show();
			$($(this).data('modal-own').modal).show(function() {
				// console.log($('.modal-own').height());
				$('.modal-own').css({'top': top+'px','left': left-($('.modal-own').width()/2)+'px', opacity: 1});
			});
		}
	});

	$("body").click(function(e){
		if($(e.target).closest('[data-modal-own], .modal-own').length != 0) return false;
  		$('.modal-own').css('opacity', 0).fadeOut();
	});

	// phone mask
	$("input[type='tel']").mask("+(9) 999-99-99");

	$("input[type='tel']").on("blur", function() {
	    var last = $(this).val().substr( $(this).val().indexOf("-") + 1 );
	    
	    if( last.length == 3 ) {
	        var move = $(this).val().substr( $(this).val().indexOf("-") - 1, 1 );
	        var lastfour = move + last;
	        
	        var first = $(this).val().substr( 0, 9 );
	        
	        $(this).val( first + '-' + lastfour );
	    }
	});

	// $("[data-modal-own]").click(function(e){
 //  		e.stopPropagation();
	// });

	// $(document).on('click', function(e) {
	// 	if($(e.target).parents('.modal-own').length == 0 && !$(e.target).hasClass('modal-own') && $('.modal-own').is(':visible')) {
	// 		$('.modal-own').hide();
	// 	}
	// });
});